# ChaCha20

**ChaCha20** je simetrični kriptografski algoritam koji se koristi za šifriranje podataka. Nastao je 2008. godine, a razvio ga je poznati kriptograf Daniel J. Bernstein kao alternativu postojećim algoritmima poput AES-a. Algoritam koristi 256-bitni ključ za šifriranje podataka, što ga čini jednim od najsigurnijih algoritama.

### Prednosti algoritma

Prva prednost kod algoritma ChaCHa20 je to što on **koristi ključ koji se ne može** generirati pomoću kriptografskog generatora sigurnosnih ključeva. Nakon što se ključ generira, algoritam koristi pseudo-slučajni broj generator za generiranje niza pseudoslučajnih brojeva koji se koriste za šifriranje podataka.

Druga prednost kod ovog algoritma je **brzina naspram drugih algoritama** i to ga čini pogodnim za primjenu u situacijama gdje nam brzina bitna, npr. kriptiranje podataka na mobilnim uređajima.

### Kombinacija sa Poly1305

ChaCha20 se često koristi u kombinaciji sa autentifikacijskom funkcijom **Poly1305**. Kombinacija obje stvari pruža sigurno i brzo šifriranje i autentifikaciju podataka. Ovaj algoritam je često upotrebljiv u komunikacijskim protokolima kako bi se osigurala sigurnost i privatnost podataka u prijenosu, a neki od protokola su: **TLS, SSH i VPN**.

<img src="https://cdn.discordapp.com/attachments/1083430278460092507/1104351437149978684/image.png">

##  AES (Advanced Encryption Standard)

AES se često koristi za šifriranje podataka, standardiziran je 2001. godine a razvili su ga belgijski kriptograf Joan Daemen i vincentijski kriptograf Vincent Rijmen. Algoritam koristi ključevu duljinu od 128, 192 ili 256 bitova za šifriranje podataka, što ga čini sigurnim i otpornim na napade brute-force. Sastoji se od 3 runde koja svaka uključuje nekoliko transformacija (zamjenu, pomak i miješanje bitova). Za 128-bitnu ključnu duljinu, AES koristi 10 rundi, za 192-bitnu ključnu duljinu, koristi 12 rundi, a za 256-bitnu ključnu duljinu, koristi 14 rundi.



## Sličnosti i razlike ChaCha20 i AES-a

Sličnosti:

- Oba algoritma su simetrična, što znači da koriste isti ključ za šifriranje i dešifriranje podataka.
- Oba algoritma se koriste za šifriranje podataka u različitim aplikacijama, poput kriptiranja podataka na mobilnim uređajima, web preglednicima, računalnim datotekama i mrežnim protokolima poput TLS i VPN-a.
- Oba algoritma su dizajnirana tako da pružaju sigurnost i zaštitu od različitih napada, uključujući brute-force napade.

Razlike:

- Ključna duljina: AES koristi ključevu duljinu od 128, 192 ili 256 bitova, dok ChaCha20 koristi samo 256-bitnu ključnu duljinu.
- Runde: AES koristi više rundi transformacija (10, 12 ili 14, ovisno o ključnoj duljini), dok ChaCha20 koristi samo 20 rundi.
- Način rada: AES koristi blokovsku šifru, dok ChaCha20 koristi strujni kriptosustav.
- Performanse: ChaCha20 je brži od AES-a na nekim platformama, posebice na mobilnim uređajima s 32-bitnim procesorima, dok je AES brži na nekim drugim platformama i kada se koriste hardverski ubrzivači.
- Sigurnost: I AES i ChaCha20 su sigurni algoritmi za kriptiranje podataka. Međutim, AES je bio testiran u širem rasponu situacija i ima veću podršku od strane industrije, dok je ChaCha20 relativno noviji algoritam i trenutno se koristi u manje aplikacija.





## Kod algoritma kroz python

Prije rada sa ovim algoritmom u pyhon-u potrebno je uključiti ChaCha20 komandom:

```python
pip install pycryptodome
```

Ovime se uključuju svi paketi koji su nam potrebni za ovaj algoritam.



### Šifriranje i dešifriranje

Sljedeći primjer pokazuje kako koristiti ChaCha  enkripciju (šifriranje) i `ChaCha denkripciju (dešifriranje).

```python
from Crypto.Cipher import ChaCha20
from Crypto.Random import get_random_bytes
from binascii import hexlify

obican_txt = b"Hello World!"

key = get_random_bytes(32)
iv = get_random_bytes(8)

print("Key:", hexlify(key).decode())
print("IV:", hexlify(iv).decode())

# Šifrat za enkripciju
cipher = ChaCha20.new(key = key, nonce = iv)

# Enkripcija
ciphertext = cipher.encrypt(obican_txt)
print("Običan tekst:", obican_txt.decode())
print("Cipher:", hexlify(ciphertext).decode())

# Šifrat za dekripciju
cipher = ChaCha20.new(key = key, nonce = iv)

# Dekripcija
recovered = cipher.decrypt(ciphertext)
print("Recovered:", recovered.decode())
```



**PRIMJER OUTPUT-a**

<img src="https://cdn.discordapp.com/attachments/1083430278460092507/1108349197297651722/image.png">



## Literatura

- https://www.cryptopp.com/wiki/ChaCha20
- https://xilinx.github.io/Vitis_Libraries/security/2019.2/guide_L1/internals/chacha20.html
- 





